
all: test_gcov

CFLAGS := -Wall -O2 -g --coverage

coverage: coverage.out
	tail -n5 $<

coverage.out : test_gcov
	./$<
	gcovr -r . --print-summary > $@

test_gcov : % : %.o
	gcc ${CFLAGS} -o $@ $< -lm -lc

test_gcov.o : %.o : %.c
	gcc ${CFLAGS} -c -o $@ $<

clean:
	rm -f test_gcov test_gcov.o test_gcov.gcda test_gcov.gcno coverage.out
