#include <stdio.h>
#include <math.h>
#include <complex.h>

int function_that_never_runs(void)
{
    return 17;
}

int main()
{
    unsigned int i;
    unsigned int k = 366001;    // large prime number
    unsigned int g = 184903;    // another large prime number
    unsigned int s = 1;
    for (i=0; i<200000000; i++) {
        s = (s*k) % g;
    }
    printf("s=%u\n", s);

    if (s % 1)
        printf("value is odd\n");
    else
        printf("value is even\n");

    return 0;
}

